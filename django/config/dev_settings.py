from .common_settings import *

DEBUG = True
SECRET_KEY = '84eujfie4nv90usnvjgn9egvosenbpsipokepsi49itsnbg5ug'

DATABASES['default']['NAME'] = 'mailape'
DATABASES['default']['USER'] = 'mailape'
DATABASES['default']['PASSWORD'] = '0000'
DATABASES['default']['HOST'] = 'localhost'
DATABASES['default']['PORT'] = '5432'


# Email settings:
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'example@gmail.com'   #replace the email
EMAIL_HOST_PASSWORD = 'password'        #replace email's password
EMAIL_USE_TLS = True
EMAIL_USE_SSL = False

MAILING_LIST_FROM_EMAIL = 'noreply@gmail.com'       #replace email
MAILING_LIST_LINK_DOMAIN = 'http://localhost:8000'  #for local usage only


# Celery
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'django-db'
