### Chapter 13 - command on cmd (windows)

cd mailape_aws
pip install awscli

## 1. aws configuration

aws configure
## Access Key ID:      xxxxxxxxxxxxxxxxxxxx   (Both key & ID shall be created in IAM of AWS console)
## Secret Access Key:  yyyyyyyyyyyyyyyyyyyyyyyyy
## Region: us-west-2   (used this region since it provides rds free tier)
## Format: JSON


## 2. Create cloud infrastructure stack
## This step creates Security groups x 3, PostgreSQL, SQS queue service, 
## IAM role and an InstanceProfile (SQS)

aws cloudformation create-stack ^
    --stack-name "infrastructure" ^
    --template-body "file://C:/Users/xxxx/Django_Project/mailape_aws/cloudformation/infrastructure.yaml" ^
    --capabilities CAPABILITY_NAMED_IAM ^ 
    --parameters "ParameterKey=MasterDBPassword,ParameterValue=0000" ^
    --region us-west-2

## check if stack is created
aws cloudformation describe-stacks --stack-name "infrastructure" --region us-west-2

## delete if needed
aws cloudformation delete-stack --stack-name "infrastructure"


## 3. Create AMI using Packer template
## This step is to create Amazon machine image, including EC2
## by indicating the local files & shell scripts

## first thing is to download a packer (I used 1.5.5.exe)
## then make file web_worker.json as book mentioned
## for the value of key "source_ami", get the suitable version at http://cloud-images.ubuntu.com/locator/ec2/
## for example, us-west-2, ubuntu 18.04 LTS, hvm:ebs-ssd, the AMI-ID recently is ami-053bc2e89490c5ab7

## then use command - aws cloudformation describe-stacks --stack-name "infrastructure" --region us-west-2
## to check rds ARN (DatabaseDNS), then replace it to the value of django_db_host
## also replace other values such as aws_access_key and django_secret_key

C:/Users/xxxxx/AWS/packer/packer build ^
    -var "aws_access_key=REPLACE_ACCESS_KEY" ^
    -var "aws_secret_key=REPLACE_SECRET_KEY" ^
    -var "django_db_password=0000" ^
    -var "django_db_host=xxx.xxx.us-west-2.rds.amazonaws.com" ^
    -var "django_secret_key=..." ^
    -var "email_host=smtp.example.com" ^
    -var "email_host_password=0000" ^
    -var "web_domain=mailape.example.com" ^
    C:/Users/xxxxx/py_django/mailape_aws/packer/web_worker.json


## 4. Create SSH key
## create ssh key; save the KeyMaterial (remove \n, and create a pem file)

aws ec2 create-key-pair --key-name mail_ape_production --region us-west-2


## 5. Create CloudFormation template 
## includes load balancer, Launch Config and AutoScaling Group
## this step will create & run EC2 instance on AWS

## yaml updated: need adding "AvailabilityZones: Fn::GetAZs: ''" in [LoadBalancer, LaunchConfig]
## use cmd "aws cloudformation describe-stacks" and replace the values of AMI and instanceProfile

aws cloudformation create-stack ^
    --stack-name "mail-ape-1-0" ^
    --template-body "file://C:/Users/xxxxxx/Django_project/mailape_aws/cloudformation/web_worker.yaml" ^
    --parameters "ParameterKey=WorkerAMI,ParameterValue=ami-0613aaec5d4396xxx" "ParameterKey=InstanceProfile,ParameterValue=arn:aws:iam::880305661xxx:instance-profile/infrastructure-SQSClientInstance-CISEOXE8Pxxx" ^
    --region us-west-2

aws cloudformation describe-stacks --stack-name "mail-ape-1-0" --region us-west-2


## 6. SSH to instance

## check instance (DNS name)
aws ec2 describe-instances ^
    --region=us-west-2 ^
    --filters="Name=tag:aws:cloudformation:stack-name,Values=mail-ape-1-0"
    
## SSH to instance
ssh -i "mail_ape_production.pem" "ubuntu@ec2-xx-xx-xx-xxx.us-west-2.compute.amazonaws.com"


## 7. SSH & initially create database

## SSH to the instance first
## then create file: /mailape/database/make_database.sh
cd /mailape/
mkdir database
cat > make_database.sh
## edit the sh file by vim or nano

export USER=master
export PASSWORD=password
export DJANGO_DB_PASSWORD=0000
export HOST=xxxxx.xxxxx.us-west-2.rds.amazonaws.com
bash database/make_database.sh

## if something does not work, try:
## 1. @$HOST/postgres --> replaced by "@$HOST/mailape", as db postgres does not exist
## 2. remove "CREATE DATABASE mailape;" if system said it is already created

source /mailape/virtualenv/bin/activate
cd /mailape/django
export DJANGO_DB_NAME=mailape
export DJANGO_DB_USER=mailape
export DJANGO_DB_PASSWORD=0000
export DJANGO_DB_HOST=xxxxx.xxxxx.us-west-2.rds.amazonaws.com
export DJANGO_DB_PORT=5432
export DJANGO_DB_LOG_FILE=/var/log/mailape/mailape.log
export DJANGO_DB_SECRET_KEY=...
export DJANGO_DB_SETTINGS_MODULE=config.production_settings
python manage.py migrate


## 8. update web_worker.yaml
## change DesiredCapacity 1 --> 2

aws cloudformation update-stack ^
    --stack-name "mail-ape-1-0" ^
    --template-body "file://C:/Users/xxxxx/Django_Project/mailape/cloudformation/web_worker.yaml" ^
    --parameters "ParameterKey=WorkerAMI,UsePreviousValue=true" "ParameterKey=InstanceProfile,UsePreviousValue=true" ^
    --region us-west-2
    
